use std::sync::{Arc, Mutex};

use axum::{
	http::StatusCode,
	response::{IntoResponse, Response},
	Extension, Json,
};
use bme280::{i2c::BME280, Measurements};
use clap::Parser;
use linux_embedded_hal::{Delay, I2CError, I2cdev};
use serde::Serialize;
use thiserror::Error;
use tower::ServiceBuilder;
use tower_http::trace::{DefaultOnFailure, TraceLayer};
use tracing::Level;

#[derive(Debug, Parser)]
struct Opts {
	/// The HTTP listen address
	#[clap(long, default_value = "0.0.0.0:8000")]
	listen_addr: String,

	/// The I²C device path (e.g. /dev/i2c-0).
	device: String,

	/// The I²C address.
	#[clap(long, default_value = "118")]
	addr: u8,

	/// The name of the weather station.
	#[clap(long)]
	name: Option<String>,

	/// The longitude of the weather station.
	#[clap(long)]
	lon: Option<f64>,

	/// The latitude of the weather station.
	#[clap(long)]
	lat: Option<f64>,

	/// The country of the weather station.
	#[clap(long)]
	country: Option<String>,
}

#[derive(Serialize)]
struct WeatherSys {
	country: String,
}

#[derive(Serialize)]
struct WeatherCoord {
	lon: f64,
	lat: f64,
}

#[derive(Serialize)]
struct WeatherMain {
	temp: f32,
	pressure: f32,
	humidity: f32,
}

#[derive(Serialize)]
struct Weather {
	coord: Option<WeatherCoord>,
	name: String,
	main: WeatherMain,
	sys: Option<WeatherSys>,
}

fn get_error_message(error: bme280::Error<I2CError>) -> String {
	match error {
		bme280::Error::CompensationFailed => "Failed to compensate a raw measurement".into(),
		bme280::Error::Bus(e) => format!("I²C bus error: {}", e.inner()),
		bme280::Error::InvalidData => "Failed to parse sensor data".into(),
		bme280::Error::NoCalibrationData => "No calibration data is available".into(),
		bme280::Error::UnsupportedChip => "Unsupported chip".into(),
		bme280::Error::Delay => "Delay".into(),
	}
}

#[derive(Debug, Error)]
pub enum Error {
	#[error("I²C bus error: {inner:?}")]
	I2C { inner: bme280::Error<I2CError> },
}

impl From<bme280::Error<I2CError>> for Error {
	fn from(inner: bme280::Error<I2CError>) -> Self {
		Error::I2C { inner }
	}
}

impl IntoResponse for Error {
	fn into_response(self) -> Response {
		match self {
			Error::I2C { inner } => {
				(StatusCode::INTERNAL_SERVER_ERROR, get_error_message(inner)).into_response()
			}
		}
	}
}

type LinuxBME280 = BME280<I2cdev>;

struct Sensor {
	bme: LinuxBME280,
}

type LockedSensor = Arc<Mutex<Sensor>>;

fn blocking_measure(bme280: &mut LinuxBME280) -> Result<Measurements<I2CError>, Error> {
	let m = bme280.measure(&mut Delay)?;
	Ok(m)
}

async fn metrics(
	Extension(options): Extension<Arc<Opts>>,
	Extension(sensor): Extension<LockedSensor>,
) -> Result<String, Error> {
	let m = {
		let mut sensor = sensor.lock().unwrap();
		tokio::task::block_in_place(|| blocking_measure(&mut sensor.bme))?
	};

	use std::fmt::Write;

	let metadata = options
		.name
		.as_ref()
		.map(|name| format!(r#"name="{name}""#))
		.unwrap_or_default();

	let mut s = String::with_capacity(256);
	writeln!(s, "bme280_temperature{{{}}} {}", metadata, m.temperature).unwrap();
	writeln!(s, "bme280_humidity{{{}}} {}", metadata, m.humidity).unwrap();
	writeln!(s, "bme280_pressure{{{}}} {}", metadata, m.pressure).unwrap();
	Ok(s)
}

async fn index(
	Extension(options): Extension<Arc<Opts>>,
	Extension(sensor): Extension<LockedSensor>,
) -> Result<Json<Weather>, Error> {
	let m = {
		let mut sensor = sensor.lock().unwrap();
		tokio::task::block_in_place(|| blocking_measure(&mut sensor.bme))?
	};

	let coord = options
		.lat
		.and_then(|lat| options.lon.map(|lon| WeatherCoord { lat, lon }));

	let sys = options
		.country
		.clone()
		.map(|country| WeatherSys { country });

	let name = options.name.as_deref().unwrap_or("BME280").to_owned();

	Ok(Json(Weather {
		coord,
		name,
		main: WeatherMain {
			temp: m.temperature,
			humidity: m.humidity,
			pressure: m.pressure / 100.0f32,
		},
		sys,
	}))
}

#[tokio::main()]
async fn main() {
	tracing_subscriber::fmt().with_max_level(Level::INFO).init();
	let opts = Opts::parse();
	let opts = Arc::new(opts);
	let addr = opts.listen_addr.parse().unwrap();

	tracing::info!(?opts, "starting");

	let i2c_bus = I2cdev::new(&opts.device).expect("open i2c");
	let mut bme280 = BME280::new(i2c_bus, opts.addr);
	//let mut delay = Delay;
	bme280.init(&mut Delay).expect("init bme280");

	let sensor = Arc::new(Mutex::new(Sensor { bme: bme280 }));
	tracing::info!("bme280 calibrated");

	let layer = ServiceBuilder::new().layer(
		TraceLayer::new_for_http()
			.on_request(())
			.on_response(())
			.on_body_chunk(())
			.on_eos(())
			.on_failure(DefaultOnFailure::new().level(Level::ERROR)),
	);

	let app = axum::Router::new()
		.route("/", axum::routing::get(index))
		.route("/metrics", axum::routing::get(metrics))
		.layer(Extension(opts))
		.layer(Extension(sensor))
		.layer(layer);

	tracing::info!(?addr, "listening on");

	axum::Server::bind(&addr)
		.serve(app.into_make_service())
		.await
		.unwrap();
}
